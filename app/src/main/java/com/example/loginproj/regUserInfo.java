package com.example.loginproj;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class regUserInfo extends AppCompatActivity {
    TextView NameView,PhoneView,RollView,BranchView,EmailView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_user_info);

        NameView=findViewById(R.id.U_name);
        PhoneView=findViewById(R.id.U_phone);
        RollView=findViewById(R.id.U_roll);
        BranchView=findViewById(R.id.U_branch);
        EmailView=findViewById((R.id.U_Email));


        //Getting the value of the shared preference
        SharedPreferences getShared = getSharedPreferences("REG",MODE_PRIVATE);
        String name = getShared.getString("str1","");
        String l_name = getShared.getString("str2","");
        String phone = getShared.getString("str3","");
        String roll =getShared.getString("str4","");
        String branch =getShared.getString("str7","");
        String email =getShared.getString("str5","");
        NameView.setText(name+" "+l_name);
        PhoneView.setText(phone);
        RollView.setText(roll);
        BranchView.setText(branch);
        EmailView.setText(email);


    }
}