package com.example.loginproj;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UserRegistration extends AppCompatActivity {
    Button sup;
    EditText Name, lName, phone,Roll,Email, PWD, CPwd,Branch;
    TextView wPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        Name=findViewById(R.id.fnmfield);
        lName=findViewById(R.id.lnmField);
        phone=findViewById(R.id.editPhone);
        Roll=findViewById(R.id.rollField);
        Email=findViewById(R.id.emailField);
        PWD=findViewById(R.id.passField);
        CPwd=findViewById(R.id.cPassField);
        Branch=findViewById(R.id.branchField);
        wPass=findViewById(R.id.wrongPass);
        sup=findViewById(R.id.btnSignUp);
        sup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String UserName = Name.getText().toString();
                String UserlName = lName.getText().toString();
                String PHONE = phone.getText().toString();
                String ROLL = Roll.getText().toString();
                String EMAIL = Email.getText().toString();
                String PassWord = PWD.getText().toString();
                String cPassword = CPwd.getText().toString();
                String BRANCH = Branch.getText().toString();

                if (PassWord.equals(cPassword)) {

                    SharedPreferences shared = getSharedPreferences("REG", MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();

                    editor.putString("str1", UserName);
                    editor.putString("str2", UserlName);
                    editor.putString("str3", PHONE);
                    editor.putString("str4", ROLL);
                    editor.putString("str5", EMAIL);
                    editor.putString("str6", PassWord);
                    editor.putString("str7", BRANCH);

                    editor.apply();
                    // NameView.setText(name);


                    Intent intent1 = new Intent(UserRegistration.this, MainActivity.class);
                    startActivity(intent1);
                }
                else{
                    wPass.setText("Password Didn't Match!");
                }

            }
        });

    }
}