package com.example.loginproj;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button lgn;
    EditText uname, psw;
    TextView sgnUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lgn=findViewById(R.id.loginBtn);
        uname=findViewById(R.id.usrEmail);
        psw=findViewById(R.id.pswField);
        sgnUp=findViewById(R.id.signUp);
        sgnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(MainActivity.this,UserRegistration.class);
                startActivity(intent);
            }
        });
        lgn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user=uname.getText().toString();
                String pass=psw.getText().toString();
                SharedPreferences getShared = getSharedPreferences("REG",MODE_PRIVATE);
                String email =getShared.getString("str5","");
                String pasw =getShared.getString("str6","");
                if(user.isEmpty() | pass.isEmpty()){
                    Toast.makeText(MainActivity.this, "Empty Field", Toast.LENGTH_SHORT).show();
                }
                else if(user.equals("abc@gmail.com") && pass.equals("123456")){
                    Intent intent =new Intent(MainActivity.this,secondPage.class);
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
                }
                else if(user.equals(email) & pass.equals(pasw)){
                    Intent intent1 = new Intent(MainActivity.this, regUserInfo.class);
                    startActivity(intent1);
                }
                else{
                    Toast.makeText(MainActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}